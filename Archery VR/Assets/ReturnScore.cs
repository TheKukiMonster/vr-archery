﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Collections;
using UnityEngine;

public class ReturnScore : MonoBehaviour {

    private Rigidbody rb;
    private GameObject go;
    private Vector3 dir;
    public string targetName;
    private GameObject target;
    private GameObject boss;
    private GameObject audience;
    private AudioSource crowdAmbience;
    public bool recurve;
    private bool scored;

    void Start()
    {
        scored = false;
        rb = GetComponent<Rigidbody>();
        go = gameObject;
        audience = GameObject.FindGameObjectWithTag("AudienceReactions");
        crowdAmbience = GameObject.FindGameObjectWithTag("Audience").GetComponent<AudioSource>();
        print("crowdambiencevolume = " + crowdAmbience.volume);

    }

    void OnCollisionEnter(Collision col)
    {
        AudioSource[] reactions = audience.GetComponents<AudioSource>();
        GameObject hitObj = col.gameObject;

        if (hitObj.name == targetName)
        {
            rb.isKinematic = true;
            Vector3 targetCentre = hitObj.GetComponent<Renderer>().bounds.center;
            ContactPoint contact = col.contacts[0];
            Vector3 contPoint = contact.point;
            float distance = Vector3.Distance(contPoint, targetCentre);

            float width = hitObj.GetComponent<Renderer>().bounds.size[0];
            var score = 0;

            if (recurve)
            {
                score = (int)(10 - Mathf.Floor(distance / (width / 10)));
            }
            else
            {
                score = (int)(10 - Mathf.Floor(distance / (width/20)));
            }
            
            print("Score = " + score);
            rb.transform.SetParent(hitObj.transform.parent);


            switch (score)
            {
                case 0:
                    PlayReaction(reactions, false);
                    SendMessage("UpdateEndScore", score);
                    break;
                case 1:
                    PlayReaction(reactions, false);
                    SendMessage("UpdateEndScore", score);
                    break;
                case 2:
                    PlayReaction(reactions, false);
                    SendMessage("UpdateEndScore", score);
                    break;
                case 3:
                    PlayReaction(reactions, false);
                    SendMessage("UpdateEndScore", score);
                    break;
                case 4:
                    PlayReaction(reactions,false);
                    SendMessage("UpdateEndScore", score);
                    break;
                case 5:
                    SendMessage("UpdateEndScore", score);
                    break;
                case 6:
                    SendMessage("UpdateEndScore", score);
                    break;
                case 7:
                    SendMessage("UpdateEndScore", score);
                    break;
                case 8:
                    SendMessage("UpdateEndScore", score);
                    break;
                case 9:
                    PlayReaction(reactions, true);
                    SendMessage("UpdateEndScore", score);
                    break;
                case 10:
                    PlayReaction(reactions, true);
                    SendMessage("UpdateEndScore", score);
                    break;
                default:
                    break;
            }
            return;


        }
        else if ((hitObj.name == "Face" || hitObj.name == "Boss") && scored == false)
        {
            scored = true;
            var score = 0;
            PlayReaction(reactions, false);
            print("Score = " + score);
            SendMessage("UpdateEndScore", score);
            print("Missed");
        }
        return;
    }

    private void FadeAmbienceDown()
    {
            crowdAmbience.volume -= 0.01F;   

    }

    private void FadeAmbienceUp()
    {

            crowdAmbience.volume += 0.01F;
            if (crowdAmbience.volume >= 0.06F)
            {
                CancelInvoke();
            }

    }

    private void PlayReaction(AudioSource[] reaction, bool reactionType)
    {

        //if reactionType = false, play missed noise
        if (!reactionType)
        {
            reaction[0].Play();
        }
        else if (reactionType)
        {
            int i = Mathf.FloorToInt(Random.Range(1f, 4f));
            reaction[i].Play();
        }


    }
}
