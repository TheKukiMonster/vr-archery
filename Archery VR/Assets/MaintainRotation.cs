﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaintainRotation : MonoBehaviour
{

    private GameObject player;
    private Vector3 lastPosition;
    private Vector3 newPosition;

	// Use this for initialization
	void Start ()
    {
        player = GameObject.Find("VRCamera");
        lastPosition = player.transform.position;
    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        Vector3 difference;
        newPosition = player.transform.position;

        difference = newPosition - lastPosition;

        this.transform.position += difference;

    }
}
