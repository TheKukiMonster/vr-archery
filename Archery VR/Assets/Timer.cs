﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

    public float timerLength;
    private float timeLeft;
    public Text textBox;
    private AudioSource timerBeep;
    private bool isEnd;
    private bool inProgress;
	
	// Update Timer every second
	void UpdateTimer () {

        if (timeLeft == 10f) {
            timeLeft -= 0.1f;
            textBox.text = timeLeft.ToString("F1");
            CancelInvoke();
            InvokeRepeating("UpdateTimerTenSeconds", 0.1f, 0.1f);
        }
        else {
            timeLeft -= 1f;
            textBox.text = convertTime(timeLeft);
        }
    }

    void Start()
    {
        timerBeep = this.GetComponent<AudioSource>();
        isEnd = false;
        inProgress = false;
    }

    void UpdateTimerTenSeconds () {

        if (timeLeft <= 0f)
        {
            textBox.text = timeLeft.ToString("F1");
            CancelInvoke();
            
            if (!isEnd)
            {
                Invoke("StartEndTimer",0.1F);
                isEnd = true;
            }
            else
            {
                isEnd = false;
                timerBeep.Play();
            }
            //PLAY A SOUND
        }
        else
        {
            timeLeft -= 0.1f;
            textBox.text = timeLeft.ToString("F1");
        }

        
    }

    void Update () {

        if (Input.GetKeyDown(KeyCode.T))
        {

            if (inProgress)
            {
                inProgress = false;
                CancelInvoke();
                print("here");
                textBox.text = "A B";
            }
            else
            {
                inProgress = true;
                isEnd = false;
                //Start ten second timer
                timerBeep.Play();
                timeLeft = 10F;
                InvokeRepeating("UpdateTimer", 0F, 1F); 

            }

                    }
        
    }

    void StartEndTimer()
    {
            timerBeep.Play();
            timeLeft = timerLength + 1;
            print("timer start");
            textBox.text = timeLeft.ToString("F1");
            InvokeRepeating("UpdateTimer", 0f, 1f);
    }

    string convertTime(float time)
    { 

        int minutes = Mathf.FloorToInt(time / 60f);
        int seconds = Mathf.FloorToInt(time - minutes * 60);

        string newTime = string.Format("{0:0}:{1:00}", minutes, seconds);

        return newTime;
    }

}
