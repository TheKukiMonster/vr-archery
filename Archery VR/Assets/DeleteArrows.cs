﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.Animations;
using UnityEditor.Experimental.UIElements;
using UnityEngine;
using Valve.VR;

public class DeleteArrows : MonoBehaviour {

    int numArrows;
    int numArrowsAI;

    private GameObject quiver;
    private GameObject plane;

    private bool atBoss;

    void Awake()
    {
        numArrows = GameObject.FindGameObjectsWithTag("Arrow").Length;
        print("numArrows = " + numArrows);

        atBoss = false;
        quiver = GameObject.Find("quiver");
        plane = GameObject.Find("Plane");
    }

    // Updates each frame
    void Update () {

        if (Input.GetKeyDown(KeyCode.Delete))
        {
            //Teleport user to boss if not already there
            if (!atBoss)
            {
                atBoss = true;
                plane.SendMessage("BossMovement", true);
            }
            else if(atBoss)
            {
                atBoss = false;
                
                if (numArrows == 1)
                {
                    SendMessage("UpdateScoreCard");
                    //Move the world forward or backwards. false = player away from boss; true = player toward boss 
                    plane.SendMessage("BossMovement", false);
                }

                numArrows--;
                Destroy(gameObject);
            }
            
        }

    }




}
