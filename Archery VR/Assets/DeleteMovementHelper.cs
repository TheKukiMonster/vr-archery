﻿using System.Collections;
using System.Collections.Generic;
using Valve.VR;
using UnityEngine;

public class DeleteMovementHelper : MonoBehaviour
{

    private GameObject world, quiver;

	// Use this for initialization
	void Start () {
        world = GameObject.Find("Plane");
        quiver = GameObject.Find("quiver");
    }

    //Move the world forward or backwards. false = player away from boss; true = player toward boss 
    private void BossMovement(bool dir)
    {
        if (dir == true)
        {
            FadeOut();
            Invoke("MovePlayerForward", 1F);
            Invoke("FadeIn", 1.1F);
        }
        else
        {
            FadeOut();
            Invoke("MovePlayerBack",1F);
            Invoke("FadeIn", 1.1F);
        }
        
    }


    private void MovePlayerForward()
    {


        world.transform.position = new Vector3(0, 0, -66.15F);
        Vector3 temp = quiver.transform.position;
        quiver.transform.position = new Vector3(temp.x, temp.y, temp.z += -16.15F);

    }

    private void MovePlayerBack()
    {
            world.transform.position = new Vector3(0, 0, -50);
            Vector3 temp = quiver.transform.position;
            quiver.transform.position = new Vector3(temp.x, temp.y, temp.z -= -16.15F);
    }

    private void FadeIn()
    {
        SteamVR_Fade.Start(Color.clear, 1F);
    }

    private void FadeOut()
    {
        SteamVR_Fade.Start(Color.black, 1F);
    }
}
