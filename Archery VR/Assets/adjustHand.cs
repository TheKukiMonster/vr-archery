﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class adjustHand : MonoBehaviour
{

    public float rotX;
    public float rotY;
    public float rotZ;

    private Transform initial;

	// Use this for initialization
    void Awake()
    {
        this.transform.localEulerAngles = new Vector3(rotX, rotY, rotZ);
    }
}
