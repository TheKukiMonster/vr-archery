﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteArrowsAI : MonoBehaviour
{

    int numArrows;
    int numArrowsAI;

    private GameObject quiver;
    private GameObject plane;

    private bool atBoss;

    void Awake()
    {
        numArrowsAI = GameObject.FindGameObjectsWithTag("Arrow AI").Length;
     //   print("numArrowsAI = " + numArrowsAI);

        atBoss = false;
        quiver = GameObject.Find("quiver");
        plane = GameObject.Find("Plane");

    }

    // Updates each frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Delete))
        {

            if (!atBoss)
            {
                atBoss = true;
            }
            else if (atBoss)
            {
                if (numArrowsAI == 1)
                {
                    SendMessage("UpdateScoreCardAI");
                    atBoss = false;
                }

                numArrowsAI--;
                Destroy(gameObject);
            }

            
        }
    }
}
