﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class CountScoreAI : MonoBehaviour
{

    static List<int> scorecard = new List<int>();
    static List<int> endScore = new List<int>();
    static int numArrows = 0;
    private static int endNumber = 0;
    private bool recurve;
    private string filePath;
    private GameObject saveObject;
    private string[] rowData;
    public int partNumber;
    public bool counting;

    // Use this for initialization
    void Awake()
    {
        saveObject = GameObject.Find("Plane");
        recurve = this.gameObject.GetComponent<ReturnScoreAI>().recurve;
        rowData = new string[44];

    }

    //Updates the endScore list
    void UpdateEndScoreAI(int score)
    {

        if (endScore.Count > 2)
        {
            endScore.Remove(endScore.Max());
        }
        endScore.Add(score);
        print("AI endScoreCount = " + endScore.Count);
        string scoreString = "AI Score: ";
        foreach (int i in endScore)
        {
            scoreString = scoreString + i + " ";
        }
        print(scoreString);
    }

    void UpdateScoreCardAI()
    {

        while (endScore.Count < 3)
        {
            endScore.Add(0);
        }

        foreach (int score in endScore)
        {
            scorecard.Add(score);
        }
        endScore.Clear();
        string scoreString = "";
        int totalScore = 0;
        int arrowsShot = 0;
        int endTotal = 0;
        foreach (int i in scorecard)
        {
            scoreString = scoreString + ", " + i.ToString();
            totalScore += i;
            endTotal += i;
            arrowsShot += 1;
        }
        print("AI playerScore (" + arrowsShot + "): " + scoreString + " - AI Avg Shot: " + ((float)totalScore / (float)arrowsShot) + " (" + ((float)totalScore / (float)arrowsShot) * 60 + ")");
        endNumber += 1;
        print("endNumber = " + endNumber);

        filePath = Application.dataPath + "/vrcheryscores.csv";
        print(filePath);
        print(System.IO.File.Exists(filePath));

        if (counting)
        {
            SaveData(endNumber, endTotal, totalScore);
        }
    }

    void SaveData(int endNumber, int endTotal, int totalScore)
    {
        int[] args = new int[4];
        args[0] = endNumber+22;
        args[1] = endTotal;
        args[2] = totalScore;
        args[3] = 0;
        saveObject.SendMessage("SaveData", args);

    }
}
