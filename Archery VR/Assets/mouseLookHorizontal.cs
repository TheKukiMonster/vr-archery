﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mouseLookHorizontal : MonoBehaviour {

    private Vector2 mouseDir;
    
    //Parent Object
    private Transform playerBody;

    //Camera Sensitivity
    private int sensX = 2;
    private int sensY = 1;

	// Use this for initialization
	void Start () {
        playerBody = this.transform.parent.transform;
	}
	
	// Update is called once per frame
	void Update () {

        Vector2 mouseDiff = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

        mouseDir += mouseDiff;
        this.transform.localRotation = Quaternion.AngleAxis(-mouseDir.y*sensY, Vector3.right);

        playerBody.localRotation = Quaternion.AngleAxis(mouseDir.x*sensX, Vector3.up);
        


    }
}
