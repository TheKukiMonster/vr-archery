﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindBow : MonoBehaviour
{

    public string usedBow;
    private GameObject sightPin;
    private GameObject sightBlock;


	// Use this for initialization
	void Start () {
		sightBlock = GameObject.Find("sightblockmoveableblock");
        sightPin = GameObject.Find("sightblockpin");
	}
	
	// Attach sight to bow and move it around
	void FixedUpdate () {

        if (Input.GetKeyDown(KeyCode.End))
        {
            GameObject bow = GameObject.Find(usedBow);

            //Make sightblock a child of the bow with set position
            this.gameObject.transform.parent = bow.transform;
            this.gameObject.transform.localPosition = new Vector3(-0.061F,0.055F,-0.059F);


            //Quaternion rotation = Quaternion.Euler(90.91599F, 293.368F, -155.76F);

            this.gameObject.transform.localEulerAngles = new Vector3(90.91599F, 293.368F, -155.76F);

        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            Vector3 pos = sightPin.transform.localPosition;
            float unit = 0.001F;
            sightPin.transform.localPosition = new Vector3(pos.x + (2*unit), pos.y, pos.z - (3*unit));
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            Vector3 pos = sightPin.transform.localPosition;
            float unit = 0.001F;
            sightPin.transform.localPosition = new Vector3(pos.x - (2*unit), pos.y, pos.z + (3 * unit));
        }

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            Vector3 pos = sightBlock.transform.localPosition;
            float unit = 0.001F;
            sightBlock.transform.localPosition = new Vector3(pos.x, pos.y + unit, pos.z);
        }

        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            Vector3 pos = sightBlock.transform.localPosition;
            float unit = 0.001F;
            sightBlock.transform.localPosition = new Vector3(pos.x, pos.y - unit, pos.z);
        }

    }
}
