﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceSetup : MonoBehaviour {

    public bool barebow;
    public GameObject barebowFace;
    public GameObject barebowFaceAI;

    public bool recurve;
    public GameObject recurveFace;
    public GameObject recurveFaceAI;

	// Use this for initialization
    void Start()
    {
        Transform parent = GameObject.Find("GameController").transform;

        if (recurve)
        {

            GameObject face = Instantiate(recurveFace, parent, true);
            face.transform.localPosition = new Vector3(0.6108994F, 2.84145F, -6.54283F);

            GameObject faceAI = Instantiate(recurveFaceAI, parent, true);
            faceAI.transform.localPosition = new Vector3(3.5F, 2.841452F, -6.542826F);
        }

        if (barebow)
        {
            GameObject face = Instantiate(barebowFace, parent, true);
            face.transform.localPosition = new Vector3(0.6108994F,2.84145F, -6.54283F);

            GameObject faceAI = Instantiate(barebowFaceAI, parent, true);
            faceAI.transform.localPosition = new Vector3(3.5F, 2.841452F, -6.542826F);


        }
    } 

}
