﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class findArrow : MonoBehaviour
{

    private GameObject arrow;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		
        if(GameObject.Find("ArrowTransform") != null)
        {

            Transform transform = GameObject.Find("ArrowTransform").transform.GetChild(0).transform;

            this.gameObject.transform.position = transform.position;
            this.gameObject.transform.rotation = transform.rotation;
        }

	}
}
