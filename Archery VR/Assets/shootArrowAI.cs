﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shootArrowAI : MonoBehaviour
{
    public float radsY;
    public float radsX;
    public float thrust;
    private Rigidbody rb;
    private bool shot = false;
    private object[][,] radsArray = new object[12][,];
    private int numArrowsAI;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        //Left, Middle, Right
        radsArray[0] = new object[,] { { 271.5F, -0.6F }, { 273.6F, 1.6F }, { 271.6F, 3.8F } };
        radsArray[1] = new object[,] { { 271.5F, -0.3F }, { 273.5F, 1.6F }, { 271.6F, 3.6F } };
        radsArray[2] = new object[,] { { 271.5F, -0.1F }, { 273.2F, 1.6F }, { 271.6F, 3.4F } };
        radsArray[3] = new object[,] { { 271.5F, 0.1F }, { 273F, 1.6F }, { 271.6F, 3.2F } };
        radsArray[4] = new object[,] { { 271.5F, 0.3F }, { 272.8F, 1.6F }, { 271.6F, 3F } };
        radsArray[5] = new object[,] { { 271.5F, 0.5F }, { 272.6F, 1.6F }, { 271.6F, 2.8F } };
        radsArray[6] = new object[,] { { 271.5F, 0.8F }, { 272.5F, 1.6F }, { 271.6F, 2.6F } };
        radsArray[7] = new object[,] { { 271.5F, 1F }, { 272.3F, 1.6F }, { 271.6F, 2.4F } };
        radsArray[8] = new object[,] { { 271.5F, 1.2F }, { 272.1F, 1.6F }, { 271.6F, 2.2F } };
        radsArray[9] = new object[,] { { 271.7F, 1.4F }, { 271.9F, 1.6F }, { 271.6F, 1.95F } };
        radsArray[10] = new object[,] { { 271.5F, 1.6F }, { 271.5F, 1.6F }, { 271.5F, 1.6F } };

    }

    void Awake()
    {
        numArrowsAI = GameObject.FindGameObjectsWithTag("Arrow AI").Length;
        print("numArrowsAI = " + numArrowsAI);
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        //Test Shots
        if (Input.GetKeyDown(KeyCode.KeypadMinus) && shot == false)
        {
            arrowMovementTest(radsY+(float)(numArrowsAI*0.005), radsX+(float)(numArrowsAI*0.001));

        }

        //Shoot miss
        if (Input.GetKeyDown(KeyCode.Keypad0) && shot == false)
        {
            arrowMovement(radsArray[0]);

        }

        //Shoot 1
        if (Input.GetKeyDown(KeyCode.Keypad1) && shot == false)
        {
            arrowMovement(radsArray[1]);
        }

        //Shoot 2
        if (Input.GetKeyDown(KeyCode.Keypad2) && shot == false)
        {
            arrowMovement(radsArray[2]);
        }

        //Shoot 3
        if (Input.GetKey(KeyCode.Keypad3) && shot == false)
        {
            arrowMovement(radsArray[3]);
        }

        //Shoot 4
        if (Input.GetKey(KeyCode.Keypad4) && shot == false)
        {
            arrowMovement(radsArray[4]);
        }

        //Shoot 5
        if (Input.GetKey(KeyCode.Keypad5) && shot == false)
        {
            arrowMovement(radsArray[5]);
        }

        //Shoot 6
        if (Input.GetKey(KeyCode.Keypad6) && shot == false)
        {
            arrowMovement(radsArray[6]);
        }

        //Shoot 7
        if (Input.GetKey(KeyCode.Keypad7) && shot == false)
        {
            arrowMovement(radsArray[7]);
        }

        //Shoot 8
        if (Input.GetKey(KeyCode.Keypad8) && shot == false)
        {
            arrowMovement(radsArray[8]);
        }

        //Shoot 9
        if (Input.GetKey(KeyCode.Keypad9) && shot == false)
        {
            arrowMovement(radsArray[9]);
        }

        //Shoot 10
        if (Input.GetKey(KeyCode.KeypadPlus) && shot == false)
        {
            arrowMovement(radsArray[10]);
        }
    }

    void arrowMovement(object[,] radsArray)
    {
            float rY = (float)radsArray[numArrowsAI-1,0];
            float rX = (float)radsArray[numArrowsAI-1,1];

            print("rY = " + rY + ", rX = " + rX);

            radsY = 270 - rY;
            radsY = -radsY * Mathf.Deg2Rad;

            radsX = rX * Mathf.Deg2Rad;
            
            rb.isKinematic = false;
            rb.AddForce(thrust * Mathf.Sin(radsX), thrust * Mathf.Sin(radsY), thrust * Mathf.Cos(radsY));
            shot = true;

        if (shot == true && rb.velocity != Vector3.zero)
        {
            transform.rotation = Quaternion.LookRotation(rb.velocity) * Quaternion.AngleAxis(-90, Vector3.right);
        }
    }

    void arrowMovementTest(float rY, float rX)
    {

        radsY = 270 - rY;
        radsY = -radsY * Mathf.Deg2Rad;

        radsX = radsX * Mathf.Deg2Rad;

        rb.isKinematic = false;
        transform.parent = null;
        rb.AddForce(thrust * Mathf.Sin(radsX), thrust * Mathf.Sin(radsY), thrust * Mathf.Cos(radsY));
        shot = true;

        if (shot == true && rb.velocity != Vector3.zero)
        {
            transform.rotation = Quaternion.LookRotation(rb.velocity) * Quaternion.AngleAxis(-90, Vector3.right);
        }
    }



}
