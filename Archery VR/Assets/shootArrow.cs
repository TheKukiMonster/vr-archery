﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shootArrow : MonoBehaviour {

    public float rads;
    public float thrust;
    private Rigidbody rb;
    private bool shot = false;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if (Input.GetMouseButtonDown(0) && shot == false)
        {
            rads = 270-transform.eulerAngles[0];
  //          rads = transform.parent.parent.eulerAngles[0];
   //         rads = rads + transform.parent.eulerAngles[0];
            rads = -rads * Mathf.Deg2Rad;
            rb.isKinematic = false;
            transform.parent = null;
            rb.AddForce(0, thrust*Mathf.Sin(rads), thrust*Mathf.Cos(rads));
            shot = true;
        }

        if(shot == true && rb.velocity != Vector3.zero){
            transform.rotation = Quaternion.LookRotation(rb.velocity) * Quaternion.AngleAxis(-90, Vector3.right);
           
        }
	}
}
