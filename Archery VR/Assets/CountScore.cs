﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml;
using UnityEditor;
using UnityEngine;


public class CountScore : MonoBehaviour
{

    static List<int> scorecard = new List<int>();
    static List<int> endScore = new List<int>();
    static int numArrows = 0;
    static int endNumber = 0;
    public int partNumber;
    public bool counting;
    private string[] rowData;
    private bool recurve;
    public int numEnds;

    private string filePath;
    private GameObject saveObject;
    

    void Awake()
    {
        saveObject = GameObject.Find("Plane");
        recurve = this.gameObject.GetComponent<ReturnScore>().recurve;
        rowData = new string[44];
        rowData[0] = partNumber.ToString();
        rowData[1] = recurve.ToString();

    }

    //Updates the endScore list
    void UpdateEndScore(int score)
    {
        if (endScore.Count > 2)
        {
            endScore.Remove(endScore.Max());
        }
        endScore.Add(score);
        print("endScore = " + endScore.Count);
        string scoreString = "Score: ";
        foreach (int i in endScore)
        {
            scoreString = scoreString + i + " ";
        }
        print(scoreString);
    }

    void UpdateScoreCard()
    {
        while(endScore.Count < 3)
        {
            endScore.Add(0);
        }

        foreach (int score in endScore)
        {
            scorecard.Add(score);
        }
        endScore.Clear();
        string scoreString = "";
        int totalScore = 0;
        int arrowsShot = 0;
        int endTotal = 0;
        foreach(int i in scorecard)
        {
            scoreString = scoreString + ", " + i.ToString();
            totalScore += i;
            endTotal += i;
            arrowsShot += 1;
        }
        print("playerScore (" + arrowsShot + "): " + scoreString + " - Avg Shot: " + ((float)totalScore/(float)arrowsShot) + " (" + totalScore/(arrowsShot*60) + ")");
        endNumber += 1;
        print("endNumber = " + endNumber);

        filePath = Application.dataPath+ "/vrcheryscores.csv";
        print(filePath);
        print(System.IO.File.Exists(filePath));

        if (counting)
        {
            SaveData(endNumber, endTotal, totalScore);
        }

    }

    void SaveData(int endNumber, int endTotal, int totalScore)
    {
        int[] args = new int[4];
        args[0] = endNumber;
        args[1] = endTotal;
        args[2] = totalScore;
        args[3] = 1;
        saveObject.SendMessage("SaveData", args);
        
    }

}
