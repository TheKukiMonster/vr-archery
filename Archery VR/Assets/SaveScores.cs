﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

public class SaveScores : MonoBehaviour
{

    private string[] rowData;
    public int partNumber;
    public int numEnds;
    private string filePath;
    private bool recurve;

    // Use this for initialization
    void Start ()
    {
        rowData = new string[46];
        rowData[0] = partNumber.ToString();
        rowData[1] = recurve.ToString();

        filePath = Application.dataPath + "/vrcheryscores.csv";
    }

    void Awake()
    {
        recurve = GameObject.Find("GameController").GetComponent<FaceSetup>().recurve;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void SaveData(int[] args)
    {

        int endNumber = args[0];
        int endTotal = args[1];
        int totalScore = args[2];
       
        // 1 = participant, 0 = AI
        int playerType = args[3];


        rowData[endNumber + 1] = endTotal.ToString();

        print("endNumber = " + endNumber);

        if (endNumber == numEnds+22)
        {
            StreamWriter outStream = System.IO.File.AppendText(filePath);
            StringBuilder sb = new StringBuilder();

            print("trying to save");

            int i = 0;
            for (i = 0; i <= numEnds + 1; i++)
            {
                sb.Append(rowData[i]);
                sb.Append(",");
            }

            sb.Append(rowData[i++] = (totalScore / (numEnds * 3)).ToString());
            sb.Append("1,");

            sb.Append(rowData[i++] = totalScore.ToString());
            sb.Append("2,");

            int j;
            for (j=i; j < rowData.Length-2; j++)
            {
                sb.Append(rowData[j]);
                sb.Append(",");
            }

            sb.Append(rowData[j++] = (totalScore / (numEnds * 3)).ToString());
            sb.Append("3,");

            sb.Append(rowData[j++] = totalScore.ToString());
            sb.Append("4,");

            outStream.WriteLine(sb);
            outStream.Close();
        }



    }
}
