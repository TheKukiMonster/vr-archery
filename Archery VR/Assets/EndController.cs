﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndController : MonoBehaviour {

    private int arrowsShot;

	// Use this for initialization
	void Start () {
        arrowsShot = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if(arrowsShot == 3){
            collectArrows();
        }
	}

    void collectArrows()
    {
        arrowsShot = 0;
        return;
    }

    void outOfTime()
    {
        arrowsShot = 0;
    }
}
